// This is the solution to Algorithms and Data Structures
// as created by Erin van der Veen (s4431200) and Oussama Danba (s4435591)
// Licensed under The MIT License (MIT)
// 
// This cpp contains the main() function and the algorithm itself

#include "assignment2.h"

void Assignment2::ReadVariables() {
	// Read the first line from the input
	std::cin >> num_products_ >> num_dividers_;

	// This is for implementation purposes
	// We noticed that certain testcases made it possible
	// to divide x products with x or more dividers
	// This is impossible, which made the program crash
	// Here we decide the maximum amount of dividers
	num_dividers_ = std::min(num_dividers_, num_products_ - 1);
}

void Assignment2::ReadProducts() {
	int cost;

	// Read the price for every product
	for (int i = 0; i < num_products_; i++) {
		std::cin >> cost;
		products_.push_back(cost);
	}
}

// RunAlgorithm contains the actual algorithm. 
// It returns nothing but prints the result on screen
void Assignment2::RunAlgorithm() {

	// A matrix is created which holds the value of every possible partition
	// from product x up to and including product y
	std::vector< std::vector<int> > partitions(num_products_);

	// P holds intermediate results to reduce the order from O(n^3) to O(n^2)
	int p;
	for (int i = 0; i < num_products_; i++) {
		p = 0;
		partitions.at(i).resize(num_products_);
		for (int j = i; j < num_products_; j++) {

			// Gets the cost of the next product in the partition
			// And adds it to p
			p += products_.at(j);

			// Round the number to the nearest 5
			if (p % 5 >= 3) {
				partitions.at(i).at(j) = p + (5 - p % 5);
			} else {
				partitions.at(i).at(j) = p - (p % 5);
			}
		}
	}

	// V is another matrix. On one axis we have the number of products
	// on the other the amount of dividers
	// The value in te matrix represents the minimum amount you have to pay
	// when you can devide x products with y dividers
	std::vector< std::vector<int> > v(num_products_);

	// The x products with 0 dividers is the same as
	// the partitions matrix's first row, from product 0 to x
	for (int i = 0; i < num_products_; i++) {
		v.at(i).push_back(partitions.at(0).at(i));
	}

	// Min is the minimum we have so far calculated
	int min;

	// Loops through all possible numbers of dividers
	for (int i = 1; i <= num_dividers_; i++) {

		// For every product, we have to calculate the minimum with i dividers
		for (int j = i; j < num_products_; j++) {

			// Min is set to an initial value, where the first products are all in
			// seperate partitions
			min = v.at(j - 1).at(i - 1) + partitions.at(j).at(j);

			// Go through all other possibilities, using the previous answers
			for (int k = i - 1; k < j; k++) {
				min = std::min(min, v.at(k).at(i - 1) + partitions.at(k + 1).at(j));
			}

			// Put the minimum in the matrix
			v.at(j).push_back(min);
		}
	}

	// Since we can use up to k dividers, we have to determine
	// with how many we actually reached a minimum
	// First we consider 0 dividers
	min = v.at(num_products_ - 1).at(0);
	for (int i = 1; i <= num_dividers_; i++) {
		// Then we consider every other amount of dividers
		min = std::min(min, v.at(num_products_ - 1).at(i));
	}

	// And finially we print the minimal cost
	std::cout << min << std::endl;
}

int main() {
	Assignment2 a2;
	a2.ReadVariables();
	a2.ReadProducts();
	a2.RunAlgorithm();
	return 0;
}

