// This is the solution to Algorithms and Data Structures
// as created by Erin van der Veen (s4431200) and Oussama Danba (s4435591)
// Licensed under The MIT License (MIT)

#ifndef ASSIGNMENT2_H
#define ASSIGNMENT2_H

// Iostream is needed for the in and output
#include <iostream>

// Vectors are used for the matrixes
#include <vector>

// Algorithm is used for min()
#include <algorithm>

int main();

class Assignment2 {
	private:
		// Contains the prices of every product
		std::vector<int> products_;
		int num_products_;
		int num_dividers_;
		int min_cost_;
	public:
		void ReadVariables();
		void ReadProducts();
		void RunAlgorithm();
};

#endif

