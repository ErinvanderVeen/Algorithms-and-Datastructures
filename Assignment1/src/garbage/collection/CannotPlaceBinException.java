/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage.collection;

/**
 *
 * @author Erin
 */
public class CannotPlaceBinException extends Exception {

    /**
     * Creates a new instance of <code>CannotPlaceBinException</code> without
     * detail message.
     */
    public CannotPlaceBinException() {
    }

    /**
     * Constructs an instance of <code>CannotPlaceBinException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public CannotPlaceBinException(String msg) {
        super(msg);
    }
}
