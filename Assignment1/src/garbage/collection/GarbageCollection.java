package garbage.collection;

/**
 *
 * @author Erin van der Veen
 * @author Oussama Danba
 */
public class GarbageCollection {

    public static void main(String[] args)
    {
        // Create the city
        City Nijmegen = new City();
        // Execute the algorithm and determine output
        if(Nijmegen.verify())
            System.out.println("possible");
        else
            System.out.println("impossible");
    }
}
