package garbage.collection;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class contains the main data structure, the graph itself. It also holds
 * the algorithm. This eliminates copying of the graph.
 * @author Erin van der Veen
 * @author Oussama Danba
 */
public class City {
    private ArrayList<Intersection> intersections = new ArrayList<>();
    private int nrStreets;
    private int nrIntersections;
    private int nrBins;
    
    // Constructor of the city, also parses the console input.
    public City()
    {
        Scanner in = new Scanner(System.in);
        while(!parseProperties(in.nextLine())){}
        
        createIntersections(nrIntersections);
       
        for(int i = 0; i < nrStreets; i++)
            while(!parseStreet(in.nextLine())){}
    }
    
    public boolean verify()
    {
        return verify(nrBins);
    }
    
    // Implementation of the algorithm.
    private boolean verify(int nrBins)
    {
        // O(1)
        if(nrBins <= 0)
            return true;
        if(nrBins > intersections.size())
            return false;
                    
        // O(n)
        Intersection i = intersections.remove(0);

        // CASE1: We assume that the node should contain a bin.
        // O(1)
        ArrayList<Intersection> removed = new ArrayList();
        
        // O(n) because it's given that an intersection has at most 4 neighbours
        for(Intersection n : i.getNeighbours())
        {
            if(intersections.remove(n))
            {
                removed.add(n);
            }
        }

        // Next recursion with in worst case 1 node less.
        if(verify(nrBins-1))
        {
            return true;
        }

        // CASE2: We assume that node i should not contain a bin.
        // O(n) because it's given that an intersection has at most 4 neighbours
        for(Intersection n : removed)
        {
            intersections.add(n);
        }

        // Next recursion with 1 node less.
        if(verify(nrBins))
        {
            return true;
        }
        
        // O(n)
        intersections.add(i);
        return false;
    }
    
    // Helper fuction of the constructor. Used to parse the first line of input.
    private boolean parseProperties(String in)
    {
        String[] input = in.split(" ");
        
        try
        {
            nrStreets = Integer.parseInt(input[0]);
            nrIntersections = Integer.parseInt(input[1]);
            nrBins = Integer.parseInt(input[2]);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    // Helper fuction of the constructor, creates and fills the ArrayList.
    // Basically makes the nodes of the graph without creating the edges.
    private void createIntersections(int nrIntersections)
    {
        for(int i = 0; i < nrIntersections; i++)
            intersections.add(new Intersection());
    }

    // Streets are the edges of the graph. This helper fuction is used to parse
    // the rest of the lines.
    private boolean parseStreet(String in)
    {
        String[] input = in.split(" ");
        int from = -1;
        int to = -1;
        
        try
        {
            from += Integer.parseInt(input[0]);
            to += Integer.parseInt(input[1]);
            intersections.get(from).addNeighbour(intersections.get(to));
            intersections.get(to).addNeighbour(intersections.get(from));
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
}
