package garbage.collection;

import java.util.ArrayList;

/**
 * This is the data structure for the intersection.
 * @author Erin van der Veen
 * @author Oussama Danba
 */
public class Intersection {
    private final ArrayList<Intersection> neighbours = new ArrayList<>();
            
    
    // Add a new neighbour, AKA create a new edge.
    public void addNeighbour(Intersection i)
    {
        neighbours.add(i);
    }
    
    public ArrayList<Intersection> getNeighbours()
    {
        return neighbours;
    }
}
